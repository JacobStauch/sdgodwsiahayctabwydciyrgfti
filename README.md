# Self-Discovery Game On Dealing With Slow Internet At Home And You Can Take A Break When Your Dog Comes Into Your Room Go Fix The Internet

aka **Go Fix The Internet**

Created for Global Game Jam 2019 at the UMSL site in St. Louis. Will receive some small Quality of Life changes, original release from the jam is linked at the bottom of the page.

## The Story

This intense self-discovery game tells the heartwarming story of a fellow (that's you!) who just wants to browse Reddit and look at cat memes. Unfortunately, you live in Nowhere, Missouri, and your internet refuses to work correctly. Fight the clock and venture down into the dark basement to restore the internet and lower your stress before you go insane. You're not alone on your quest, however, as your trusty, rave-loving canine companion will come to your aid when you need him the most!

## The Theme

The theme for Global Game Jam 2019 was "What Home Means to You". Given that a good portion of our development team lives where there's no good internet, this game was inevitable.

## Credits

Austin Froese - Programming, Level Design

Sarah Lester - Programming

Jacob Stauch - 3D Modeling, Programming

Ben High - Programming

Raven Graham - Voice Acting

Jacob McGowen - Texture Artist

## [Legacy Release](https://globalgamejam.org/2019/games/go-fix-internet)