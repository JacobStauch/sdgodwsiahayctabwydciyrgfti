﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class RadioController : MonoBehaviour {
    public AudioClip[] clips;
    private AudioSource _audioSource;
    public AudioClip currentClip;

    private void Start() {
        _audioSource = GetComponent<AudioSource>();
    }

    public void Activate() {
        currentClip = clips[Random.Range(0, clips.Length - 1)];
        _audioSource.clip = currentClip;
        _audioSource.Play();
    }
}
