﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleporterController : MonoBehaviour {
    private void OnCollisionEnter(Collision other) {
        if (other.gameObject.gameObject.gameObject.gameObject.CompareTag("Player")) {
            SceneManager.LoadScene(2);
        }
    }
}
