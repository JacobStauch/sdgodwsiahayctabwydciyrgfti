﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
    public PlayerController player;
    public float stress;
    public float maxStress = 50;
    public float maxInternetSpeed = 100;
    public float internetStressStart = 30;
    public float internetSpeed;
    public float timeDecay = 2;
    public bool gameLost = false;
    public PointAtObject pointingAt = PointAtObject.None;

    [FormerlySerializedAs("_tvAudio")] public AudioSource tvAudio;
    [FormerlySerializedAs("_gameOverSound")] public AudioSource gameOverSound;

    private void Start() {
        tvAudio = GameObject.FindWithTag("TV").GetComponent<AudioSource>();
        player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
        internetSpeed = maxInternetSpeed;
    }

    private void Update() {
        internetSpeed = Mathf.Clamp(internetSpeed - Time.deltaTime * timeDecay, 0, maxInternetSpeed);

        if (internetSpeed <= internetStressStart) {
            stress = Mathf.Clamp(stress + Time.deltaTime, 0, maxStress);
        }

        if (gameLost == false && stress == maxStress) {
            gameLost = true;
            StartCoroutine(gameOverSequence());
        }
    }

    IEnumerator gameOverSequence()
    {
        gameOverSound.Play();
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(1);
    }

    public void Interact(Collider interactCollider) {
        switch (pointingAt) {
            case PointAtObject.Router:
                internetSpeed = maxInternetSpeed;
                break;
            case PointAtObject.Tv:
                tvAudio.Play();
                break;
            case PointAtObject.Door:
                interactCollider.gameObject.GetComponent<DoorController>().Interact();
                break;
            case PointAtObject.Radio:
                interactCollider.gameObject.GetComponent<RadioController>().Activate();
                break;
            case PointAtObject.None:
                Debug.Log("But nothing happened!");
                break;
        }
    }
}
