﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DogMovement : MonoBehaviour
{
    public Transform dogHouse;
    public Transform player;
    private NavMeshAgent doggo;
    private Bounds bound;
    public float max;
    public float min;
    private int waitTime = 5;
    public bool isDogFollowing;
    public bool touchedPlayer;
    private GameController _gameMaster;

    // Start is called before the first frame update
    void Start() {
        _gameMaster = GameObject.FindWithTag("GameMaster").GetComponent<GameController>();
        isDogFollowing = false;
        doggo = GetComponent<NavMeshAgent>();
        doggo.destination = dogHouse.position;
        StartCoroutine(moveTowardsPlayer());
    }

    IEnumerator moveTowardsPlayer()
    {
        yield return new WaitForSeconds(Random.Range(min, max));
        isDogFollowing = true;
    }

    IEnumerator moveTowardsDogHouse()
    {
        yield return new WaitForSeconds(waitTime);
        isDogFollowing = false;
        touchedPlayer = false;
    }

    // Update is called once per frame
    void Update()
    {
        bound = new Bounds(doggo.transform.position, new Vector3(1, 2, 1));
        if (bound.Contains(player.transform.position)) {
            doggo.destination = dogHouse.position;
            touchedPlayer = true;
            _gameMaster.stress = 0;
            StartCoroutine(moveTowardsDogHouse());
        }
        if (bound.Contains(dogHouse.position) && touchedPlayer == false) {
            StartCoroutine(moveTowardsPlayer());
        }
        if (isDogFollowing == true) {
            doggo.destination = player.position;
        }
    }
}