﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UiController : MonoBehaviour {
    public bool paused;
    public GameController gameManager;

    public RawImage stressMeter;
    public RawImage internetMeter;
    public Text interactText;

    public Canvas pauseCanvas;

    private void Start() {
        gameManager = GameObject.FindWithTag("GameMaster").GetComponent<GameController>();
    }

    private void Update() {
        // Adjust cursor state
        Cursor.lockState = paused ? CursorLockMode.None : CursorLockMode.Locked;
        Cursor.visible = paused;
        
        // Check for pause toggle
        if (Input.GetButtonDown("Cancel")) {
            paused = !paused;
        }

        // Update stress meter
        stressMeter.transform.localScale = new Vector3(gameManager.stress / gameManager.maxStress, 1, 1);
        
        // Update internet speed meter
        internetMeter.rectTransform.eulerAngles =
            -Vector3.Lerp(new Vector3(180, 0, 270), new Vector3(180, 0, 90), gameManager.internetSpeed / gameManager.maxInternetSpeed);
        
        // Update pointing at message
        interactText.enabled = gameManager.pointingAt != PointAtObject.None;
        
        // Update paused menu
        if (paused) {
            pauseCanvas.enabled = true;
            Time.timeScale = 0f;

            foreach (AudioSource sources in FindObjectsOfType<AudioSource>()) {
                sources.Pause();
            }
        } else {
            pauseCanvas.enabled = false;
            Time.timeScale = 1f;
            
            foreach (AudioSource sources in FindObjectsOfType<AudioSource>()) {
                sources.UnPause();
            }
        }
    }

    public void Resume() {
        paused = false;
    }

    public void Quit() {
        Application.Quit();
    }
}
