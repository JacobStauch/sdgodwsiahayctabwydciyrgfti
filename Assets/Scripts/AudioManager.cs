﻿using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class name is not quite accurate.
public class AudioManager : MonoBehaviour
{
    public Transform player;
    private AudioSource audioSource;
    public AudioClip[] sounds;
    private Bounds bound;
    private bool isPlaying = false;
    private GameController _gameController;
    public float momStress = 10;

   
    void Awake(){
      audioSource = gameObject.GetComponent<AudioSource>();
    }

    private void Start() {
        _gameController = GameObject.FindWithTag("GameMaster").GetComponent<GameController>();
    }

    IEnumerator playVoice() {
        isPlaying = true;
        int index = Random.Range(0, sounds.Length);
        audioSource.clip = sounds[index];
        audioSource.Play();
        _gameController.stress = Mathf.Clamp(_gameController.stress + momStress, 0, _gameController.maxStress);
        yield return new WaitForSeconds(audioSource.clip.length);
        isPlaying = false;
    }

    void Update(){
        bound = new Bounds(this.transform.position, new Vector3(2,3,2));
        //play a random audio clip when close to mom
        if (bound.Contains(player.transform.position) && isPlaying == false) {
            StartCoroutine(playVoice());
        }

    }
}
