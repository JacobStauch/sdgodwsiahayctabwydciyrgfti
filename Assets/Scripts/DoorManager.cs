﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorManager : MonoBehaviour
{
    Animator anim;
    Quaternion doorOpen = Quaternion.Euler(0,180,0);
    Quaternion doorClose = Quaternion.Euler(0,90,0);
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void OpenDoor()
    { 
            //check if door is open or closed 
            anim.SetTrigger("OpenDoor");
  
            anim.SetTrigger("CloseDoor");
            
            
    }
   
}
