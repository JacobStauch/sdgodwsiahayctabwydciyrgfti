﻿using System;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public Transform playerCamera;
    
    public float walkSpeed = 5;
    public float xSensitivity = 35;
    public float ySensitivity = 35;
    public float joystickBoost = 3;

    public float interactDistance = 4;

    public float boundsSize = 5;
    public float computerStressReliefMod = 3;
    public float slowInternetFrustration = 1;
    public float tvAudioStressReliefMod = 1.5f;

    public GameController gameController;

    private Transform _computer;
    private Transform _tv;

    private void Start() {
        gameController = GameObject.FindWithTag("GameMaster").GetComponent<GameController>();
        _computer = GameObject.FindWithTag("Computer").transform;
        _tv = GameObject.FindWithTag("TV").transform;
    }

    private void FixedUpdate() {
        string xControl = Mathf.Abs(Input.GetAxis("Mouse X")) > Mathf.Abs(Input.GetAxis("sx")) ? "Mouse X" : "sx";
        string yControl = Mathf.Abs(Input.GetAxis("Mouse Y")) > Mathf.Abs(Input.GetAxis("sy")) ? "Mouse Y" : "sy";
        
        transform.Translate(Input.GetAxis("Horizontal") * walkSpeed * Time.deltaTime,
                            0,
                            Input.GetAxis("Vertical") * walkSpeed * Time.deltaTime);
        transform.Rotate(0, Input.GetAxis(xControl) * xSensitivity * (xControl == "sx" ? joystickBoost : 1) * Time.deltaTime, 0);
        playerCamera.Rotate(-Input.GetAxis(yControl) * ySensitivity * (yControl == "sy" ? joystickBoost : 1) * Time.deltaTime, 0, 0);
    }

    private void Update() {
        // Update stress
        if (transform.position.y < 0) {
            gameController.stress += Time.deltaTime;
        }
        
        // Check for interactable objects
        gameController.pointingAt = PointAtObject.None;
        RaycastHit interactRay;
        if (Physics.Raycast(playerCamera.position, playerCamera.transform.forward, out interactRay, interactDistance, ~(1 << 9))) {
            Interactable interactable = interactRay.transform.gameObject.GetComponent<Interactable>();
            
            if (interactable != null) {
                gameController.pointingAt = interactable.objectType;
            }
        }
        
        // Send the gameController an interact message when interacting
        if (Input.GetButtonDown("Interact")) {
            gameController.Interact(interactRay.collider);
        }
        
        // Check for being around computer
        Bounds playerBounds = new Bounds(transform.position, new Vector3(boundsSize, 2, boundsSize));
        if (playerBounds.Contains(_computer.position)) {
            if (gameController.internetSpeed > gameController.internetStressStart) {
                gameController.stress =
                    Mathf.Clamp(gameController.stress - Time.deltaTime * computerStressReliefMod, 0,
                        gameController.maxStress);
            } else {
                gameController.stress =
                    Mathf.Clamp(gameController.stress + Time.deltaTime * slowInternetFrustration, 0,
                        gameController.maxStress);
            }
        }
        
        // Check if near the TV
        if (playerBounds.Contains(_tv.position) && gameController.tvAudio.isPlaying) {
            gameController.stress = Mathf.Clamp(gameController.stress - Time.deltaTime * tvAudioStressReliefMod,
                0, gameController.maxStress);
        }
    }
}
