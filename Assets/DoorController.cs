﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class DoorController : MonoBehaviour {
    private Animator _animator;
    public bool open;
    private static readonly int Open = Animator.StringToHash("Open");

    private void Start() {
        _animator = GetComponent<Animator>();
        open = false;
    }

    public void Interact() {
        open = !open;
        _animator.SetBool(Open, open);
    }
}
