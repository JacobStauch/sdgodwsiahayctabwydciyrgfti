﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CautionController : MonoBehaviour {
    public void Continue() {
        GetComponent<Canvas>().enabled = false;
    }

    public void Back() {
        SceneManager.LoadScene(1);
    }

    private void Update() {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        
        // I'm just gonna put this in here, don't worry about it
        if (Input.GetButtonDown("Cancel")) {
            Back();
        }
    }
}
