﻿using UnityEngine;

public class DoggoRotato : MonoBehaviour {
    public float rotatoSpeed = 50;
    
    private void Update() {
        transform.Rotate(0, rotatoSpeed * Time.deltaTime, 0);
    }
}
