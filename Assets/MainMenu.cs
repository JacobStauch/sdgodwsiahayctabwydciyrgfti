﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {
    public RawImage fadeOutObject;
    public Animator fadeOutAnimator;
    private static readonly int Fading = Animator.StringToHash("Fading");

    public void QuitGame() {
        fadeOutObject.enabled = true;
        fadeOutAnimator.SetBool(Fading, true);
        StartCoroutine(QuitGameActual());
    }

    IEnumerator QuitGameActual() {
        yield return new WaitForSeconds(1.5f);
        Application.Quit();
        Debug.Log("Application Quit");
    }
    
    public void PlayGame() {
        fadeOutObject.enabled = true;
        fadeOutAnimator.SetBool(Fading, true);
        Debug.Log("Play");
        StartCoroutine(PlayGameActual());
    }

    IEnumerator PlayGameActual() {
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(1);
    }
}
